const Product = require("../models/Product.js");

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.createProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product({
		productName : data.product.productName,
		productImage: data.productImage,
		productType: data.product.productType,
		description : data.product.description,
		price : data.product.price
	});

	return newProduct.save().then((newProduct, error) => {
		if (error){
			return `Product creation failed. Try again.`;
		}

			return `New product is successfully created!`;
	})
}

	// if user is not admin, then return this message as promise to avoid error.
	let message = Promise.resolve('You must be an ADMIN to create a product.')

	return message.then((value) => {
		return value
	})

}

module.exports.getActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

module.exports.getRings = () => {
	return Product.find({productType : "Ring"}).then(result => {
		return result;
	})
}

module.exports.getNecklaces = () => {
	return Product.find({productType : "Necklace"}).then(result => {
		return result;
	})
}

module.exports.getEarrings = () => {
	return Product.find({productType : "Earring"}).then(result => {
		return result;
	})
}

module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (productId, data) => {

	if(data.isAdmin){

	return Product.findByIdAndUpdate(productId, {
		productName: data.product.productName,
		description: data.product.description,
		price: data.product.price
	}).then((updatedProduct, error) => {
		if(error){
			return false;
		} else {
			return true;
		}

	
	})

} let message = Promise.resolve('You must be an ADMIN to update a product.')

	return message.then((value) => {
		return value
	})
}

module.exports.archiveProduct = (productId, data) => {

	if(data.isAdmin){

	return Product.findByIdAndUpdate(productId, {
		isActive : false
	}) 
	.then((archivedProduct, error) => {
		if(error){
			return false;
		} else {
			return true;
		}

	
	})

}

let message = Promise.resolve('You must be an ADMIN to archive this product.')

	return message.then((value) => {
		return value
	})

}

module.exports.unarchiveProduct = (productId, data) => {

	if(data.isAdmin){

	return Product.findByIdAndUpdate(productId, {
		isActive : true
	}) 
	.then((unarchivedProduct, error) => {
		if(error){
			return false;
		} else {
			return true;
		}

	
	})

}

let message = Promise.resolve('You must be an ADMIN to unarchive this product.')

	return message.then((value) => {
		return value
	})

}
