const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {
		type : String,
		required : [true, "Name is required."]
	},
	productImage : {
		type : String,
		required : [true, "Image is required."]
	},
	productType : {
		type: String,
		required: [true, "Product Type is required"]
	},
	description : {
		type : String,
		required: [true, "Description is required."]
	},
	price : {
		type : Number,
		required : [true, "Price is required."]
	},
	isActive : {
		type : Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}

});

module.exports = mongoose.model("Product", productSchema)