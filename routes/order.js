const express = require("express");
const router = express.Router();
const Order = require("../models/Order.js");
const User = require("../models/User.js")
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");


// non-admin create order
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId,
		quantity : req.body.quantity
		// products : [{
		// 	productId : req.body.productId,
		// 	quantity : req.body.quantity
		// }]
	}

	orderController.checkout(data).then(resultFromController => res.send(
		resultFromController))
})


module.exports = router;

router.get("/all", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}



	orderController.getAllOrders(data).then(resultFromController => {
		res.send(resultFromController)
	})
})


